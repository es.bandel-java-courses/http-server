package org.example.framework.http;

import lombok.Builder;
import lombok.RequiredArgsConstructor;
import lombok.Singular;
import lombok.extern.slf4j.Slf4j;
import org.example.framework.exception.*;
import org.example.framework.handler.Handler;
import org.example.framework.middleware.Middleware;
import org.example.framework.util.Bytes;


import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.SSLSocket;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
@Builder
@RequiredArgsConstructor
public class Server {
    private static final int MAX_REQUEST_LINE_AND_HEADERS_SIZE = 4096;
    private static final byte[] CRLF = new byte[]{'\r', '\n'};
    private static final byte[] CRLFCRLF = new byte[]{'\r', '\n', '\r', '\n'};
    private static final int MAX_CONTENT_LENGTH = 10 * 1024 * 1024;

    @Singular
    private final List<Middleware> middlewares;
    @Singular
    private final Map<Pattern, Map<HttpMethods, Handler>> routes;
    @Builder.Default
    private final Handler notFoundHandler = Handler::notFoundHandler;
    @Builder.Default
    private final Handler methodNotAllowed = Handler::methodNotAllowedHandler;
    @Builder.Default
    private final Handler internalServerErrorHandler = Handler::internalServerError;
    @Builder.Default
    private final Handler userNotAuthorizedHandler = Handler::userNotAuthorizedHandler;
    @Builder.Default
    private final Handler noAccessHandler = Handler::noAccessHandler;

    public void serve(final Ports port) throws IOException {
        try (
                final ServerSocket serverSocket = (port == Ports.HTTPS ? SSLServerSocketFactory
                        .getDefault()
                        .createServerSocket(port.value())
                        : new ServerSocket(port.value()));

        ) {

            if (port == Ports.HTTPS) {
                final SSLServerSocket sslServerSocket = (SSLServerSocket) serverSocket;
                sslServerSocket.setEnabledProtocols(new String[]{"TLSv1.2"});
                sslServerSocket.setWantClientAuth(true);
            }
            log.info("server listen on {}", port.value());
            while (true) {
                try (
                        final Socket socket = (port == Ports.HTTPS ? (SSLSocket) serverSocket.accept() : serverSocket.accept());
                        final InputStream in = new BufferedInputStream(socket.getInputStream());
                        final OutputStream out = socket.getOutputStream();
                ) {
                    try {
                        final Request request = new Request();

                        final byte[] buffer = new byte[MAX_REQUEST_LINE_AND_HEADERS_SIZE];
                        if (!in.markSupported()) {
                            throw new MarkNotSupportedException();
                        }
                        in.mark(MAX_REQUEST_LINE_AND_HEADERS_SIZE);

                        final int firstRead = in.read(buffer);
                        // TODO: \r\n
                        final int requestLineEndIndex = Bytes.indexOf(buffer, CRLF);
                        if (requestLineEndIndex == -1) {
                            throw new InvalidRequestStructureException("request line end index not found");
                        }
                        log.debug("request line end index: {}", requestLineEndIndex);
                        final String requestLine = new String(buffer, 0, requestLineEndIndex, StandardCharsets.UTF_8);
                        log.debug("request line: {}", requestLine);
                        final String[] requestLineParts = requestLine.split("\\s+", 3);
                        if (requestLineParts.length != 3) {
                            throw new InvalidRequestLineStructureException(requestLine);
                        }

                        request.setMethod(requestLineParts[0]);
                        request.setHttpVersion(requestLineParts[2]);

                        final String pathAndQuery = URLDecoder.decode(requestLineParts[1], StandardCharsets.UTF_8.name());

                        final String[] pathAndQueryParts = pathAndQuery.split("\\?", 2);

                        final String requestPath = pathAndQueryParts[0];
                        request.setPath(requestPath);
                        if (pathAndQueryParts.length == 2) {
                            request.setQuery(pathAndQueryParts[1]);
                        }
                        final int headersStartIndex = requestLineEndIndex + CRLF.length;
                        final int headersEndIndex = Bytes.indexOf(buffer, CRLFCRLF, headersStartIndex);
                        if (headersEndIndex == -1) {
                            throw new InvalidRequestStructureException("headers end index wasn't found");
                        }

                        int lastProcessedIndex = headersStartIndex;
                        int contentLength = 0;
                        while (lastProcessedIndex < headersEndIndex - CRLF.length) {
                            final int currentHeaderEndIndex = Bytes.indexOf(buffer, CRLF, lastProcessedIndex);
                            log.debug("current header end index: {}", currentHeaderEndIndex);
                            final String currentHeaderLine = new String(buffer, lastProcessedIndex,
                                    currentHeaderEndIndex - lastProcessedIndex);
                            log.debug("current header line: {}", currentHeaderLine);
                            lastProcessedIndex = currentHeaderEndIndex + CRLF.length;
                            final String[] headerParts = currentHeaderLine.split(":\\s*", 2);
                            if (headerParts.length != 2) {
                                throw new InvalidRequestStructureException(currentHeaderLine);
                            }
                            request.getHeaders().put(headerParts[0], headerParts[1]);
                            if (!headerParts[0].equalsIgnoreCase(HttpHeaders.CONTENT_LENGTH.value())) {
                                continue;
                            }
                            contentLength = Integer.parseInt(headerParts[1]);
                            log.debug("content length: {}", contentLength);
                        }
                        if (contentLength > MAX_CONTENT_LENGTH) {
                            throw new RequestBodyTooLargeException();
                        }
                        final int bodyStartIndex = headersEndIndex + CRLFCRLF.length;
                        in.reset();
                        final long skipped = in.skip(bodyStartIndex);

                        final byte[] body = new byte[contentLength];
                        final int bodyRead = in.read(body);
                        request.setBody(body);
                        in.reset();

                        log.debug("client connected: {}:{}", socket.getInetAddress(), socket.getPort());
                        if (port == Ports.HTTPS) {
                            for (final Middleware middleware : middlewares) {
                                middleware.handle(socket, request);
                            }

                        }

                        Map<HttpMethods, Handler> methodToHandlers = null;
                        for (final Map.Entry<Pattern, Map<HttpMethods, Handler>> entry : routes.entrySet()) {
                            final Matcher matcher = entry.getKey().matcher(requestPath);
                            if (!matcher.matches()) {
                                continue;
                            }
                            request.setPathMatcher(matcher);
                            methodToHandlers = entry.getValue();
                        }

                        try {
                            if (methodToHandlers == null) {
                                throw new MethodNotAllowedException(request.getMethod());
                            }

                            final Handler handler = methodToHandlers.get(HttpMethods.valueOf(request.getMethod()));
                            if (handler == null) {
                                throw new ResourceNotFoundException(requestPath);
                            }
                            handler.handle(request, out);
                        } catch (MethodNotAllowedException e) {
                            log.error("request method not allowed", e);
                            methodNotAllowed.handle(request, out);
                        } catch (ResourceNotFoundException e) {
                            log.error("can't found request", e);
                            notFoundHandler.handle(request, out);
                        } catch (UserNotAuthorizedException e) {
                            log.error("user is not authorized", e);
                            userNotAuthorizedHandler.handle(request, out);
                        } catch (NoAccessException e) {
                            log.error("user is not author", e);
                            noAccessHandler.handle(request, out);
                        } catch (Exception e) {
                            log.error("can't handle request", e);
                            internalServerErrorHandler.handle(request, out);
                        }
                    } catch (Exception e) {
                        log.error("can't handle request", e);
                        internalServerErrorHandler.handle(new Request(), out);
                    }
                } catch (Exception e) {
                    log.error("some error", e);
                }
            }
        }
    }
}

