package org.example.framework.http;

import lombok.*;

import java.security.Principal;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Matcher;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class Request {
    private String method;
    private String path;
    private Matcher pathMatcher;
    private String query;
    private final Map<String, List<String>> queryParams = new LinkedHashMap<>();
    private String httpVersion;
    @Singular
    private final Map<String, String> headers = new LinkedHashMap<>();
    private byte[] body;
    private Principal principal;

    public String getPathGroup(String name) {
        return pathMatcher.group(name);
    }

    public String getPathGroup(int index) {
        return pathMatcher.group(index);
    }

    public Optional<String> getQueryParam(String name) {
        // Just demo realization
        return Optional.empty(); // Optional.ofNullable()
    }

    public String getQueryParamOrNull(String name) {
        return null;
    }

    public List<String> getQueryParams(String name) {
        throw new RuntimeException();
    }

    public Map<String, List<String>> getAllQueryParams() {
        return queryParams;
    }
}
