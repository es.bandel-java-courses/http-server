package org.example.framework.http;

public enum Ports {

    HTTP(8080),
    HTTPS(8443);

    private final int value;

    Ports(int value) {
        this.value = value;
    }

    public int value() {
        return this.value;
    }
}
