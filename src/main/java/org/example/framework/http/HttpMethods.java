package org.example.framework.http;

public enum HttpMethods {
  GET,
  POST,
  PUT,
  DELETE;
}
